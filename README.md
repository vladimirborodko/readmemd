# VideoComfort iOS client

## Building

---

### Preprocessor flags:

  * -D APNS_ENVIRONMENT to specify APNS environment used by app. default: "staging".
  * -D VC_DEBUG to make some debug features available (environment switch, for example).

---

## Contributing

---

Чтобы поддерживать [Непрерывную интеграцию][CI] необходимо работать итерациями, поэтому мы построим свой [Scrum][Scrum] с планированием и мерж реквестами.

### Git workflow

Гит в скраме выглядит примерно так:

<img src=".gitflare.png">

Вот основные гитожители:
* [master][master] - ветка истории релизов. Живет в [origin][origin] перманентно.
Принимает только git merge --no-ff из [develop][develop] или [waterfall][waterfall]
* [develop][develop] - ветка истории разработки. Живет в [origin][origin] перманентно.
Принимает только git merge -ff-only от [release][release] 
(либо [waterfall][waterfall] в случае если [waterfall][waterfall] не может быть влит в [sprint][sprint])
* [release#{номер релиза}][release] - ветка подготовки релизов. Бренчуется от [sprint][sprint] в конце итерации.
В нее попадают только мерж реквесты из [bugfix][bugfix]
* [waterfall#{номер релиза}][release] - ветка экстренного релиза. Бренчуется от [sprint][sprint] в конце итерации.
В нее попадают только git merge --no-ff из [hotfix][hotfix]
* [sprint#{номер итерации}][sprint] - ветка интеграции фичей. В начале итерации бренчуется от [develop][develop]. 
Принимает только мерж реквесты из [feature][feature] и git merge --no-ff из [waterfall][waterfall]
В конце итерации вливается в [develop][develop], и удаляется
* Ветка [feature/{номер тикета}-{короткое_емкое_описание}][feature] бренчуется от текущего [sprint-#][sprint].
Содержит сорс в процессе работы, по готовности вливается в [sprint-#][sprint] и удаляется.
* [{фамилия}/{креатив владельца}][worker] - ветки разработчиков, отсюда выставляются мерж реквесты в фичевые ветки.
Когда реквест апрувится ветка удаляется автоматически.
* Тэг [{код продукта}/${major}.${minor}(${build})][tag] ставится коммиту со сборкой которая успешно легла в стор
* Тэг [{код продукта}/stage/{major}.{minor}.{build}][tag] ставится коммиту со сборкой релиз кандидата
* Тэг [{код продукта}/feature/{номер тикета}/{номер попытки}][tag] ставится коммиту с фичей которую необходимо протестировать

[origin]: (https://git.restr.im/videocomfort-mobile/vc-ios)
[CI]: (https://ru.wikipedia.org/wiki/Непрерывная_интеграция)
[Scrum]: (https://ru.wikipedia.org/wiki/Scrum)
[master]: (https://git.restr.im/videocomfort-mobile/vc-ios/tree/master)
[develop]: (https://git.restr.im/videocomfort-mobile/vc-ios/tree/develop)
[sprint]: ()
[release]: ()
[feature]: ()
[worker]: ()
[tag]: ()
[waterfall]: ()
[bugfix]: ()
[hotfix]: ()

### Code style

Для начала [Raywenderlich](https://github.com/raywenderlich/swift-style-guide)
Однажды появится статический анализатор кода

### Unit testing

TDD голубая мечта разработчика, Kent Beck утверждает что этот сниль ничуть не замедляет процесс
